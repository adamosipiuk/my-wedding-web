import smtplib
import ssl
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart


def send_email(party, guests):
    sender_email = "paskudy@paskudy2021.pl"
    receivers_email = ["osipiukadam@gmail.com", "katarzyna.malawska@o2.pl", "michal.l.osipiuk@gmail.com"]
    smtp_server = 'poczta22517.domeny.host'
    smtp_port = 465
    password = "lubiePlacki1500"

    message = MIMEMultipart("alternative")
    message["Subject"] = f'Wesele - {party.name}'
    message["From"] = sender_email

    for receiver_email in receivers_email:
        message["To"] = receiver_email

        # Create the plain-text and HTML version of your message
        if party.is_attending_ceremony:
            is_attending_ceremony = 'przybędą'
        else:
            is_attending_ceremony = 'nie przybędą'
        html_table = """<table style="width:100%">
                        <tr>
                            <th>Imię</th>
                            <th>Nazwisko</th> 
                            <th>Przybędzie</th>                        
                            <th>Menu</th>                  
                            <th>Nocleg</th>                                       
                            <th>Poprawiny</th>
                        </tr>"""
        if party.is_attending_ceremony:
            for guest in guests:
                html_table = f'''{html_table}
                                <tr>
                                    <th>{guest.first_name}</th>
                                    <th>{guest.last_name}</th> 
                                    <th>{guest.is_attending_ceremony}</th>                        
                                    <th>{guest.meal}</th>                  
                                    <th>{guest.is_accommodation_needed}</th>                                       
                                    <th>{guest.is_attending_aftermath}</th>
                                </tr>'''
            html_table = f'{html_table}</table>'
        else:
            html_table = ''
        text = f'''<h2><b>{party.name}</b> podjeli decyzję, iż <b>{is_attending_ceremony}</b> na wesele.<br>
         Ich adres e-mail to:<br> <b>{party.email}</b>.<br>
         Dodali komentarz:<br> <b>{party.comments}</b><br>'''
        # html = f'{json2html.convert(json = guests)}'
        html = f'<body> {text} <br> {html_table} </body>'
        # Turn these into plain/html MIMEText objects
        # part1 = MIMEText(text, "plain")
        part2 = MIMEText(html, "html")

        # Add HTML/plain-text parts to MIMEMultipart message
        # The email client will try to render the last part first
        # message.attach(part1)
        message.attach(part2)

        # Create secure connection with server and send email
        context = ssl.create_default_context()
        with smtplib.SMTP_SSL(smtp_server, smtp_port, context=context) as server:
            server.login(sender_email, password)
            server.sendmail(
                sender_email, receiver_email, message.as_string()
            )