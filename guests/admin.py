from django.contrib import admin
from .models import Guest, Party


class GuestInline(admin.TabularInline):
    model = Guest
    fields = ('first_name', 'last_name', 'is_attending_ceremony', 'is_accommodation_needed', 'is_attending_aftermath',
              'meal', 'is_child')
    readonly_fields = ('first_name', 'last_name', 'email')


class PartyAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'category', 'save_the_date_sent', 'invitation_sent', 'rehearsal_dinner', 'invitation_opened',
                    'is_invited', 'is_attending_ceremony', 'email')
    list_filter = ('type', 'category', 'is_invited', 'is_attending_ceremony', 'rehearsal_dinner', 'invitation_opened')
    inlines = [GuestInline]


class GuestAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'party', 'email', 'is_attending_ceremony', 'is_child', 'meal',
                    'is_accommodation_needed', 'is_attending_aftermath')
    list_filter = ('is_attending_ceremony', 'is_child', 'meal', 'party__category',
                   'party__rehearsal_dinner')


admin.site.register(Party, PartyAdmin)
admin.site.register(Guest, GuestAdmin)
