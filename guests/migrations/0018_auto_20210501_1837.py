# Generated by Django 2.2.20 on 2021-05-01 18:37

from django.db import migrations, models
import guests.models


class Migration(migrations.Migration):

    dependencies = [
        ('guests', '0017_auto_20210501_1828'),
    ]

    operations = [
        migrations.RenameField(
            model_name='party',
            old_name='is_attending',
            new_name='is_attending_ceremony',
        ),
        migrations.AlterField(
            model_name='guest',
            name='email',
            field=models.TextField(blank=True, max_length=30, null=True),
        ),
        migrations.AlterField(
            model_name='guest',
            name='first_name',
            field=models.TextField(max_length=20),
        ),
        migrations.AlterField(
            model_name='guest',
            name='is_accommodation_needed',
            field=models.NullBooleanField(default=None),
        ),
        migrations.AlterField(
            model_name='guest',
            name='last_name',
            field=models.TextField(blank=True, max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='party',
            name='invitation_id',
            field=models.CharField(db_index=True, default=guests.models._random_uuid, max_length=5, unique=True),
        ),
    ]
