# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2021-05-02 21:29
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('guests', '0019_auto_20210502_2127'),
    ]

    operations = [
        migrations.AddField(
            model_name='guest',
            name='unknown_name',
            field=models.TextField(blank=True, max_length=20, null=True),
        ),
    ]
