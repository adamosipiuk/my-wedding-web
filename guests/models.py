from __future__ import unicode_literals
import datetime
import uuid

from django.db import models
from django.dispatch import receiver

# these will determine the default formality of correspondence
ALLOWED_TYPES = [
    ('rodzina_adama', 'rodzina_adama'),
    ('rodzina_kasi', 'rodzina_kasi'),
    ('znajomi_adama', 'znajomi_adama'),
    ('znajomi_kasi', 'znajomi_kasi'),
    ('randomy', 'randomy'),
    ('gospodarze', 'gospodarze'),
]


def _random_uuid():
    return uuid.uuid4().hex[:5].upper()


class Party(models.Model):
    """
    A party consists of one or more guests.
    """
    name = models.TextField()
    type = models.CharField(max_length=15, choices=ALLOWED_TYPES)
    category = models.CharField(max_length=20, null=True, blank=True)
    save_the_date_sent = models.DateTimeField(null=True, blank=True, default=None)
    save_the_date_opened = models.DateTimeField(null=True, blank=True, default=None)
    invitation_id = models.CharField(max_length=5, db_index=True, default=_random_uuid, unique=True)
    invitation_sent = models.DateTimeField(null=True, blank=True, default=None)
    invitation_opened = models.DateTimeField(null=True, blank=True, default=None)
    is_invited = models.BooleanField(default=False)
    rehearsal_dinner = models.BooleanField(default=False)
    is_attending_ceremony = models.NullBooleanField(default=None)
    email = models.TextField(null=True, blank=True, max_length=35)
    comments = models.TextField(null=True, blank=True)

    def __str__(self):
        return 'Party: {}'.format(self.name)

    @classmethod
    def in_default_order(cls):
        return cls.objects.order_by('category', '-is_invited', 'name')

    @property
    def ordered_guests(self):
        return self.guest_set.order_by('is_child', 'pk')

    @property
    def any_guests_attending(self):
        return any(self.guest_set.values_list('is_attending_ceremony', flat=True))

    @property
    def guest_emails(self):
        return list(filter(None, self.guest_set.values_list('email', flat=True)))


MEALS = [
    ('meat', 'standardowego'),
    ('vegetarian', 'wegetariańskiego'),
]


class Guest(models.Model):
    """
    A single guest
    """
    party = models.ForeignKey('Party', on_delete=models.CASCADE)
    unknown_name = models.BooleanField(default=False)
    first_name = models.TextField(null=True, blank=True, max_length=20)
    last_name = models.TextField(null=True, blank=True, max_length=20)
    email = models.TextField(null=True, blank=True, max_length=30)
    is_attending_ceremony = models.NullBooleanField(default=None)
    meal = models.CharField(max_length=20, choices=MEALS, null=True, blank=True)
    is_child = models.BooleanField(default=False)
    is_accommodation_needed = models.NullBooleanField(default=None)
    is_attending_aftermath = models.NullBooleanField(default=None)

    @property
    def name(self):
        return u'{} {}'.format(self.first_name, self.last_name)

    @property
    def unique_id(self):
        # convert to string so it can be used in the "add" templatetag
        return str(self.pk)

    def __str__(self):
        return 'Guest: {} {}'.format(self.first_name, self.last_name)
